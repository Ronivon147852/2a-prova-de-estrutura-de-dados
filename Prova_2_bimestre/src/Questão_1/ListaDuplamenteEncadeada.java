package Questão_1;

public class ListaDuplamenteEncadeada {
    private No prim;
    private No ult;
    private int quantiNo;

    //Métodos -----------------------------------------
    public ListaDuplamenteEncadeada(){
        this.prim = null;
        this.ult = null;
        this.quantiNo = 0;
    }
    public No getPrim() {
        return prim;
    }
    public void setPrim(No prim) {
        this.prim = prim;
    }
    public No getUlt() {
        return ult;
    }
    public void setUlt(No ult) {
        this.ult = ult;
    }
    public int getQuantiNo() {
        return quantiNo;
    }
    public void setQuantiNo(int quantiNo) {
        this.quantiNo = quantiNo;
    }


    public void inserirPrimeiro(Object dado){
        No novoNo = new No(dado);
        if (eVazia()){
            this.prim = novoNo;
            this.ult = novoNo;
            this.quantiNo++;
        }else {
            this.prim.setAnt(novoNo);
            novoNo.setProx(this.prim);
            this.prim = novoNo;
            this.quantiNo++;
        }
    }
    public void inserirUltimo(Object p){
        No novoNo = new No(p);
        if (eVazia()){
            this.prim = novoNo;
            this.ult = novoNo;
            quantiNo++;
        }
        else{
            ult.setProx(novoNo);
            novoNo.setAnt(ult);
            this.ult = novoNo;
            this.quantiNo ++;
        }
    }
    public void inserirNoMeio(int indice, Object p){
        No novoNo = new No(p);
        No atual = this.prim;
        No ant = null;
        if (eVazia()){
            this.prim = novoNo;
        }
        else{
            if (indice < 0 || indice >= quantiNo){
                System.out.println("Indice fora dos parâmetros!");
            }else {
                if (indice == 0) {
                    novoNo.setProx(this.prim);
                    this.prim.setAnt(novoNo);
                    this.prim = novoNo;
                } else {
                    for (int i = 0; i < quantiNo; i++) {
                        if (i != indice) {
                            ant = atual;
                            atual = atual.getProx();
                        } else {
                            novoNo.setAnt(ant);
                            ant.setProx(novoNo);
                            novoNo.setProx(atual);
                            novoNo.getProx().setAnt(novoNo);
                            quantiNo++;
                            break;
                        }
                    }
                }
            }
        }
    }
    public boolean removerNo(Object dado){
        No atual = prim;
        if (dado == prim.getDado()){
            prim = prim.getProx();
            return true;
        }
        if (dado == ult.getDado()){
            ult = ult.getAnt();
            ult.setProx(null);
            return true;
        }
        for (int i = 0; i < quantiNo-2; i++) {
            atual = atual.getProx();
            if (atual.getDado() == dado){
                atual.getAnt().setProx(atual.getProx());
                atual.getProx().setAnt(atual.getAnt());
                return true;
            }
        }
        return false;
    }
    public boolean removerNoInicio(){
        prim = prim.getProx();
        quantiNo--;
        return true;
    }
    public boolean removerNoFinal(){
        ult = ult.getAnt();
        ult.setProx(null);
        quantiNo--;
        return true;
    }
    public int pesquisarNo(Object elemento){
        int indice = 0;
        No atual = this.prim;
        while((atual != null) && (!atual.equals(elemento))){
            atual = atual.getProx();
            indice++;
        }
        if (atual != null){
           return indice;
        }
        return -1;
    }
    public String imprimirLista(){
        String msg="";
        if (eVazia()){
            msg = "A Lista está vazia!";
        }else{
            No atual = this.prim;
            while(atual != null){
                msg += atual.getDado() + " -> ";
                atual = atual.getProx();
            }
        }
        return msg;
    }
    public void imprimir(){
        No atual = this.prim;
        while (atual!=null) {
            System.out.print("No{Dado = " + atual.getDado() + " Próximo = ");
            atual = atual.getProx();
        }
    }
    public void imprimirInverso(){
        No atual = prim;
        System.out.print(" Nó{" + atual.getDado() + ", Anterior = null");
        atual = atual.getProx();
        for (int i = 0; i < quantiNo-1; i++) {
            System.out.print(" Nó{" + atual.getDado() + ", Anterior = " + atual.getAnt().getDado());
            atual = atual.getProx();
        }
        /*No atual = this.ult;
        System.out.print("No{Dado = " + atual.getDado() + " Anterior = ");
        while (atual.getAnt() != null){
            System.out.print("No{Dado = " + atual.getAnt().getDado() + " Anterior = ");
            atual = atual.getAnt();
            if (atual.getAnt() == null)
                System.out.print("null}}}}}}");
        }*/
    }
    public boolean eVazia(){
        return (this.prim == null);
    }

    @Override
    public String toString() {
        return "ILista{" +
                "prim=" + prim +
                ", ult=" + ult +
                ", quantiNo=" + quantiNo +
                '}';
    }
}
