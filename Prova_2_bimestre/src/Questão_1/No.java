package Questão_1;

public class No {

    private Object dado;
    private No prox;
    private No ant;

    public No getAnt() {
        return ant;
    }

    public void setAnt(No ant) {
        this.ant = ant;
    }


    public Object getDado() {
        return dado;
    }
    public No(Object pe){
        this.dado = pe;
        this.prox = null;
        this.ant = null;
    }

    public void setDado(int dado) {
        this.dado = dado;
    }

    public No getProx() {
        return prox;
    }

    public void setProx(No prox) {
        this.prox = prox;
    }

    @Override
    public String toString() {
        return "No{" + "dado = " + dado + " Próximo = ";
    }
}
