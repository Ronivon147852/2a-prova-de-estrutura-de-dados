package Questão_2;

public class Fila {
    private No inicio, fim;
    private int quantidadeNo = 0;


    //MÉTODO UTILIZADO PARA A PROVA -----------
    public int remover(){
        if (inicio == null){
            return -1;
        }
        int removido = inicio.getDado();
        if (inicio != null) {
            inicio = inicio.getProx();
            quantidadeNo--;
        }
        return removido;
        //FIM DO MÉTODO UTILIZADO PARA PROVA--------------
    }


    public void inserir(int dado) {
        No novoNo = new No(dado);
        if (eVazia()){
            inicio = novoNo;
            fim = novoNo;
        }
        else{
            this.fim.setProx(novoNo);
            fim = novoNo;
        }
        quantidadeNo++;
    }

    public String exibirFila(){
        if (eVazia()){
            return ("lista Vazia");
        }
        String buffer = "";
        No atual = inicio;
        System.out.print("Prim: ");
        while (atual != null){
            buffer += atual.getDado() + " , Prox: ";
            atual = atual.getProx();
        }
        buffer += "null";
        return buffer;
    }
    private boolean eVazia(){
        if (quantidadeNo == 0)
            return true;
        return false;
    }

    private No getInicio() {
        return inicio;
    }

    private void setInicio(No inicio) {
        this.inicio = inicio;
    }

    public int getQuantidadeNo() {
        return quantidadeNo;
    }

    @Override
    public String toString() {
        return "Questão_2.Fila{" +
                "prim=" + inicio +
                ", ult=" + fim +
                ", quantidadeNo=" + quantidadeNo +
                '}';
    }
}
