package Questão_2;

import Questão_1.Validar_HTML;

public class Main {
    public static void main(String[] args) {
        Fila f = new Fila();
        f.inserir(1);
        f.inserir(2);
        f.inserir(3);
        f.inserir(4);

        Pilha p = new Pilha();
        p.inverterFila(f);
        p.imprimir();
    }
}
