package Questão_2;

public class No {

    private int dado;
    private No prox;

    public int getDado() {
        return dado;
    }
    public No(int pe){
        this.dado = pe;
        this.prox = null;
    }

    public void setDado(int dado) {
        this.dado = dado;
    }

    public No getProx() {
        return prox;
    }

    public void setProx(No prox) {
        this.prox = prox;
    }

    @Override
    public String toString() {
        return "Questão_2.No{" +
                "dado=" + dado +
                ", prox=" + prox +
                '}';
    }
}
