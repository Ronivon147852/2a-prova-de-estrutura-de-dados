package Questão_2;

public class Pilha {
    private No topo;
    private int quantidadeDeDados = 0;



    //MÉTODO DA PROVA------------------------

    public void inverterFila(Fila fila){
        while(fila.getQuantidadeNo() != 0) {
                empilhar(fila.remover());
        }
    }


    //FIM DO MÉTODO DA PROVA-----------------


    public void empilhar(int dado){
        No novoNo = new No(dado);
        if (eVazia()){
            setTopo(novoNo);
        }else{
            novoNo.setProx(getTopo());
            setTopo(novoNo);
        }
        quantidadeDeDados++;
    }

    public Object desempilhar(){
        No removido = getTopo();
        setTopo(getTopo().getProx());
        quantidadeDeDados--;
        return removido.getDado();
    }
    public void imprimir(){
        No atual = getTopo();
        while (atual != null){
            System.out.println("|" + atual.getDado() + "|");
            atual = atual.getProx();
        }
        System.out.print("¨¨¨");
    }

    private boolean eVazia(){
        if (quantidadeDeDados == 0)
            return true;
        return false;
    }

    public No getTopo() {
        return topo;
    }

    public void setTopo(No topo) {
        this.topo = topo;
    }

    public int getQuantidadeDeDados() {
        return quantidadeDeDados;
    }

    public void setQuantidadeDeDados(int quantidadeDeDados) {
        this.quantidadeDeDados = quantidadeDeDados;
    }

}
